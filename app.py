import requests
import simplejson as json
import json
from flask import Flask, request, Response
from flask import send_from_directory
from flask import send_file
import io
from flask_cors import CORS
from flask_restplus import Api, Resource, inputs, fields, marshal
import re
import boto3
from io import StringIO


# config
app = Flask(__name__)
app.config["PROPAGATE_EXCEPTIONS"] = False
app.config.SWAGGER_UI_DOC_EXPANSION = 'full'
api = Api(app, version='1.0', title='frivol_Api')

# parser for frivol-login
parser = api.parser()
parser.add_argument('token', type=str, required=False,
                    help='Bearer Access Token.')

parser.add_argument(
    "username", type=str, required=True, help="username")

parser.add_argument(
    "password", type=str, required=True, help="password")

parser.add_argument(
    "cookie", type=str, required=False, help="anything works")


# parser for frivol-Check
parser_frivol_check = api.parser()
parser_frivol_check.add_argument('token', type=str, required=False,
                               help='Bearer Access Token.')

parser_frivol_check.add_argument(
    "key", type=str, required=True, help="cookie object")


# parser for frivol-send-message
parser_frivol_send_message = api.parser()

parser_frivol_send_message.add_argument('token', type=str, required=False,
                               help='Bearer Access Token.')

parser_frivol_send_message.add_argument(
    "JSON-string", type=str, required=False, help="JSON-string")                               




# parser for get-megssage-frivol
parser_frivol_get_message = api.parser()
parser_frivol_get_message.add_argument('token', type=str, required=False,
                               help='Bearer Access Token.')

parser_frivol_get_message.add_argument(
    "key", type=str, required=True, help="key")

parser_frivol_get_message.add_argument(
    "url", type=str, required=True, help="url")

# define for auth
parser_credentials = api.parser()

parser_credentials.add_argument(
    "username", type=str, required=True, help="username")

parser_credentials.add_argument(
    "password", type=str, required=True, help="password")

CORS(app, origins=r'*', allow_headers=r'*')

# models
token = fields.String(example='"eyJraWQiOiI5M2NHeEY5dHhINTl2SWJpVDR1NDZQQ0VvOFNGZ2xEdno5THg1UVRcL2k3Yz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJjOWRmNTUzMC0zOGQ3LTQzMTEtODM5Zi00OThmNTMzZDU0ZmYiLCJjb2duaXRvOmdyb3VwcyI6WyJiYXNpY3BsYW4iXSwiZXZlbnRfaWQiOiI5MzA2Yjk3NC1hZWE3LTQ4MGYtYjY4Yi1kZDhjZTkzMzhmZmMiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIiwiYXV0aF90aW1lIjoxNjAyMDY1NDEzLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV9uZTYwczlEUXkiLCJleHAiOjE2MDIwNjkwMTMsImlhdCI6MTYwMjA2NTQxMywianRpIjoiN2QxYjI4YmYtYmIxNS00MzcyLWFiYjktN2ExZmRmMDUzOWM2IiwiY2xpZW50X2lkIjoiM3JzanRidHNmMmJ2NWYxODdkOTk4OWQyc2wiLCJ1c2VybmFtZSI6ImM5ZGY1NTMwLTM4ZDctNDMxMS04MzlmLTQ5OGY1MzNkNTRmZiJ9.RS-tvI0-Fdb8gvJAxvr2jVSs3ChhPAlXEGGkJu2EoA28PgVdKFfqui9IRzHtGVZi4CDpRs8ZAsA7g7FApF5bJmONnWeu2WWpghMsuEZFixiaEfVlCajhukMZZZC_KiAyxCdKGwti5_DbvWpmGBwdqdlQOWOLhZFPCm2XgnxFp_3ANrqZEJhXwGDTekxl-AMyb1L1s-msE6-5EgVqv9ZggTg9eEwNFc_VfKw87ahDP8sK9Qgz6ZPTsozhCo9vXMQ-YfAlAAicIiK-nJoSwo3fhjLMF-rgcFPySDS9ePWKb08vJfyWLp3-itZHwmn6d5ULKJkE0Q8e-5SSUNfUIX1dxQ"')

# login für frivol


@api.route('/frivol-login', doc={"description": "Login at frivol with frivol credentials"})
class Loginfrivol(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser)
    def post(self):
        args = parser.parse_args()

        # don't check auth on local host
        #req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        #if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # Log into the chat
        isLogged = login_frivol(args["username"], args["password"], args["cookie"])

        return isLogged

# login frivol parameter l for login-id and p for password


def login_frivol(profile, password, anyCookie):

    url = "https://www.frivol.com/account/login/check"

    payload={'form[username]': profile,
    'form[password]': password,
    'form[remember_me]': '1',
    'submit': '',
    'form[channel]': 'member',
    'form[_token]': 'BvMlS9pSTEjmmUFicgGpz8x3GQjE596Vr3HYUS-VuoQ'}
    files=[

    ]
    headers = {
    'method': 'POST',
    'Accept': 'Application/json',
    'Cookie': 'frvsess={}'.format(anyCookie)
    }

    response = requests.request("POST", url, headers=headers, data=payload, files=files)

    return response.url == "https://www.frivol.com/amateurcenter/"


# check für frivol
@api.route('/frivol-check', doc={"description": "check for new messages"})
class Checkfrivol(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_frivol_check)


    def post(self):
        args = parser_frivol_check.parse_args()

        # don't check auth on local host
        #req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        #if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = check_msg_frivol(args['key'])

        return rslt

# check for new messages, parameter c is cookie, if no message end here, else get_html
def check_msg_frivol(key):
    url = "https://www.frivol.com/messenger/list/1?filter=unanswered&limit=10"

    anyCookie = json.loads(key)['anyCookie']
    REMEMBERME = json.loads(key)['REMEMBERME']


    payload={}
    files={}
    headers = {
    'method': 'GET',
    'scheme': 'https',
    'accept': '*/*',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
    'cookie': '_ga=GA1.2.809859057.1614684515; _gid=GA1.2.856051302.1614684515; frvsess={}; REMEMBERME={}'.format(anyCookie, REMEMBERME),
    'referer': 'https://www.frivol.com/messenger/',
    'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
    'x-requested-with': 'XMLHttpRequest'
    }

    response = requests.request("GET", url, headers=headers, data=payload, files=files)
    jsn = json.loads(response.text)

    if jsn['total'] <= 0:
        return "No unread"
    else:
        arr = []
        openMsg = jsn['inbox']
        for message in openMsg:
            arr.append(get_message_frivol(message['id'], anyCookie, REMEMBERME))
    return arr
            
            


#get messages of Frivol
def get_message_frivol(customerID, anyCookie, REMEMBERME):
    url = "https://www.frivol.com/messenger/conversation/{}/now".format(customerID)

    payload={}
    headers = {
      'method': 'GET',
      'accept': 'application/json, text/javascript, */*; q=0.01',
      'cookie': '_ga=GA1.2.809859057.1614684515; _gid=GA1.2.856051302.1614684515; frvsess={}; REMEMBERME={}'.format(anyCookie, REMEMBERME),

    }

    response = requests.request("POST", url, headers=headers, data=payload)
    jsn = json.loads(response.text)
    return jsn



# senden für frivol
@api.route('/frivol-send-message', doc={"description": "send message"})
class getMessagefrivol(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_frivol_send_message)
    def post(self):
        args = parser_frivol_send_message.parse_args()
        payload = request.get_json()


        # don't check auth on local host
        #req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        #if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = frivol_send_message(payload)


#send message
def frivol_send_message(jsn):
    url = "https://www.frivol.com/messenger/message/reply/{}".format(jsn['customerId'])

    payload={'id': '',
    'is_new': 'true',
    'is_from_myself': 'true',
    'is_bought': 'false',
    'message': jsn['message'],
    'created_at': jsn['date'],
    'attachment_costs': '0'}
    files=[

    ]
    headers = {
    'authority': 'www.frivol.com',
    'method': 'POST',
    'path': '/messenger/message/reply/{}'.format(jsn['customerId']),
    'scheme': 'https',
    'accept': '*/*',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'cookie': '_ga=GA1.2.809859057.1614684515; _gid=GA1.2.856051302.1614684515; frvsess={}; REMEMBERME={};'.format(jsn['key']['anyCookie'], jsn['key']['REMEMBERME']),
    'origin': 'https://www.frivol.com',
    'referer': 'https://www.frivol.com/messenger/',
    'sec-ch-ua': '"Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'x-requested-with': 'XMLHttpRequest'
    }

    response = requests.request("POST", url, headers=headers, data=payload, files=files)


# auth
@api.route("/oauth/token", doc={
    "description": "Returns a Bearer authentication token. The token is valid for 30 minutes."
})
class awsAuth(Resource):
    @api.expect(parser_credentials)
    @api.response(200, 'Success', token)
    @api.response(401, 'Unauthorized Error')
    def get(self):
        args = parser_credentials.parse_args()
        username = args["username"]
        password = args["password"]
        try:
            client = boto3.client("cognito-idp", "eu-central-1")
            response = client.initiate_auth(
                AuthFlow="USER_PASSWORD_AUTH",
                AuthParameters={"USERNAME": username, "PASSWORD": password, },
                ClientId="41ft5d2kpm37ctefvbf0pb3m29",
                ClientMetadata={"UserPoolId": "eu-central-1_o8ZrmNcQP"},
            )
        except:
            raise Unauthorized('That email/password combination is not valid.')

        Token = response["AuthenticationResult"]["AccessToken"]
        return Token


# get user
def get_user_name(token):
    region = "eu-central-1"
    provider_client = boto3.client("cognito-idp", region_name=region)
    try:
        user = provider_client.get_user(AccessToken=token)
    except provider_client.exceptions.NotAuthorizedException as e:
        raise Unauthorized('Invalid token!')
    return user["Username"]


if __name__ == "__main__":
    app.run()
